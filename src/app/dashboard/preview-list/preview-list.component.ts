import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Video } from '../../app.types';

@Component({
  selector: 'abc-preview-list',
  templateUrl: './preview-list.component.html',
  styleUrls: ['./preview-list.component.css']
})
export class PreviewListComponent {
  @Input() videoList: Video[] = [];
  @Input() selectedVideoId: string | undefined;
  @Output() setVideo = new EventEmitter<Video>();

  selectVideo(video: Video) {
    this.setVideo.emit(video);
  }
}

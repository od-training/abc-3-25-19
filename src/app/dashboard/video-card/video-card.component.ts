import { Component, Input } from '@angular/core';
import { Video } from '../../app.types';

@Component({
  selector: 'abc-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.css']
})
export class VideoCardComponent {
  @Input() video: Video | undefined;
  constructor() {}
}

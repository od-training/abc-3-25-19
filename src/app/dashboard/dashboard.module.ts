import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { PreviewListComponent } from './preview-list/preview-list.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { FilterViewsComponent } from './filter-views/filter-views.component';
import { VideoCardComponent } from './video-card/video-card.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [{ path: '', component: DashboardComponent }];

@NgModule({
  declarations: [
    PreviewListComponent,
    VideoPlayerComponent,
    FilterViewsComponent,
    VideoCardComponent,
    DashboardComponent
  ],
  imports: [CommonModule, RouterModule.forChild(routes), ReactiveFormsModule]
})
export class DashboardModule {}

import { Component } from '@angular/core';
import { Observable, Subject, merge } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { Video } from '../app.types';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'abc-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  videos: Observable<Video[]>;
  selectedVideo: Observable<Video>;
  searchTerm: Observable<string>;

  private videoChanged = new Subject<Video>();

  constructor(private ds: DashboardService) {
    this.videos = ds.getVideos().pipe(shareReplay(1));
    this.searchTerm = ds.searchTerm;

    this.selectedVideo = merge(
      this.videos.pipe(map(videos => videos[0])),
      this.videoChanged
    );
  }

  setVideo(video: Video) {
    this.videoChanged.next(video);
  }

  updateTerm(term: string) {
    this.ds.updateTerm(term);
  }
}

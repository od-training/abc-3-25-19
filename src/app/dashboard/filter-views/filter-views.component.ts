import {
  Component,
  OnDestroy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, map, distinctUntilChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'abc-filter-views',
  templateUrl: './filter-views.component.html',
  styleUrls: ['./filter-views.component.css']
})
export class FilterViewsComponent implements OnDestroy {
  @Input() set searchTerm(value: string) {
    if (value) {
      this.search.setValue(value);
    }
  }
  @Output() updateTerm = new EventEmitter<string>();

  search = new FormControl('');
  private termSub: Subscription;

  constructor() {
    this.termSub = this.search.valueChanges
      .pipe(
        debounceTime(270),
        map(term => term.trim()),
        distinctUntilChanged()
      )
      .subscribe(term => {
        this.updateTerm.emit(term);
      });
  }

  ngOnDestroy() {
    this.termSub.unsubscribe();
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Video } from '../app.types';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap, shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  searchTerm: Observable<string>;
  historyMap = new Map<string, Observable<Video[]>>();

  constructor(
    private http: HttpClient,
    private router: Router,
    activatedRoute: ActivatedRoute
  ) {
    this.searchTerm = activatedRoute.queryParams.pipe(
      map(params => params['q'])
    );
  }

  getVideos(): Observable<Video[]> {
    return this.searchTerm.pipe(
      switchMap(term => {
        const cachedResult = this.historyMap.get(term);
        if (cachedResult) {
          return cachedResult;
        }

        this.historyMap.set(
          term,
          this.http.get<Video[]>('https://api.angularbootcamp.com/videos').pipe(
            map(list =>
              term ? list.filter(video => video.title.includes(term)) : list
            ),
            shareReplay(1)
          )
        );
        return this.historyMap.get(term) as Observable<Video[]>;
      })
    );
  }

  updateTerm(term: string) {
    void this.router.navigate([], {
      queryParams: { q: term }
    });
  }
}

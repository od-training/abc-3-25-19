import { Component, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

import { Video } from 'src/app/app.types';

const urlPrefix = 'https://www.youtube.com/embed/';

@Component({
  selector: 'abc-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {
  @Input() set video(value: Video | undefined) {
    if (value) {
      this.videoTitle = value.title;
      this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(
        urlPrefix + '/' + value.id
      );
    }
  }

  videoTitle = '';
  videoUrl: SafeResourceUrl | undefined;

  constructor(private domSanitizer: DomSanitizer) {}
}
